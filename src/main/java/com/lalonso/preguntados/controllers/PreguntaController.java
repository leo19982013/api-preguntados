package com.lalonso.preguntados.controllers;
import com.lalonso.preguntados.dto.PreguntaDTO;
import com.lalonso.preguntados.models.PreguntaModel;
import com.lalonso.preguntados.services.PreguntaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class PreguntaController {

    private PreguntaService preguntaService;

    @Autowired
    public PreguntaController(
            PreguntaService preguntaService)
    {
        this.preguntaService = preguntaService;
    }

    @GetMapping("/preguntas")
    public ArrayList<PreguntaModel> getAllPreguntas()  {

        return preguntaService.obtenerPreguntas();
    }

    @PostMapping("/preguntas")
    public PreguntaModel createPregunta(@RequestBody PreguntaDTO preguntadto)  {
        return this.preguntaService.crearPregunta(preguntadto);
    }

    @GetMapping("/preguntas/{id}")
    public Optional<PreguntaModel> getPregunta(@PathVariable Long id)  {
        return preguntaService.obtenerPreguntaPorId(id);
    }

    @DeleteMapping("/preguntas/{id}")
    public void borrarPregunta(Long id){
        preguntaService.borrarPregunta(id);
    }

}
