package com.lalonso.preguntados.controllers;
import com.lalonso.preguntados.dto.PreguntaDTO;
import com.lalonso.preguntados.dto.Pregunta_RespuestaDTO;
import com.lalonso.preguntados.dto.ValidacionRespuestaDTO;
import com.lalonso.preguntados.models.PreguntaModel;
import com.lalonso.preguntados.services.PreguntadosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class PreguntadosController {

    private PreguntadosService preguntadosService;
    @Autowired
    public void PreguntaController(PreguntadosService preguntadosService){
        this.preguntadosService = preguntadosService;
    }


    @GetMapping("/preguntados/next")
    public Pregunta_RespuestaDTO traerPreguntaRandom()  {

        return preguntadosService.ObtenerPregunta();
    }
    @GetMapping("/preguntados/verificaciones/{id}")
    public ValidacionRespuestaDTO verificarRespuesta(@PathVariable(value = "id",required = true) Long id)  {

        return preguntadosService.validarRespuesta(id);
    }
}
