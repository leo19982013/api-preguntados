package com.lalonso.preguntados.repositories;

import com.lalonso.preguntados.models.PreguntaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PreguntaRepository extends JpaRepository<PreguntaModel, Long>, JpaSpecificationExecutor<PreguntaModel> {


}
