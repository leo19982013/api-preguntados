package com.lalonso.preguntados.repositories;

import com.lalonso.preguntados.models.RespuestaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RespuestaRepository extends JpaRepository<RespuestaModel,Long> {

    @Query(value="SELECT * FROM respuestas WHERE pregunta_id = (?1)",nativeQuery = true)
    List<RespuestaModel> finAllByPreguntaId(Long pregunta_id);
}
