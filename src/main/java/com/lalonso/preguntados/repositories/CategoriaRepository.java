package com.lalonso.preguntados.repositories;
import com.lalonso.preguntados.models.CategoriaModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaRepository extends JpaRepository<CategoriaModel, Long> {


}
