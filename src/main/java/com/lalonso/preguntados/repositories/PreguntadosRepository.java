package com.lalonso.preguntados.repositories;

import com.lalonso.preguntados.models.PreguntaModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PreguntadosRepository extends JpaRepository<PreguntaModel,Long> {
}
