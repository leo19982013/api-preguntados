package com.lalonso.preguntados.services;

import com.lalonso.preguntados.dto.CategoriaDTO;
import com.lalonso.preguntados.models.CategoriaModel;
import com.lalonso.preguntados.repositories.CategoriaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class CategoriaService {

    private CategoriaRepository categoriaRepository;


    public CategoriaService(CategoriaRepository categoriaRepository) {

        this.categoriaRepository = categoriaRepository;

    }
    public ArrayList<CategoriaModel> obtenerCategorias(){
        return (ArrayList<CategoriaModel>) categoriaRepository.findAll();
    }

    public CategoriaModel crearCategoria(CategoriaDTO categoriaDTO) {

        CategoriaModel categoriaModel = new CategoriaModel();
        categoriaModel.setDescripcion(categoriaDTO.getDescripcion());
        categoriaModel.setNombre(categoriaDTO.getNombre());
        return  categoriaRepository.save(categoriaModel);
    }

    public Optional<CategoriaModel> obtenerCategoriaId(Long id) {
        return categoriaRepository.findById(id);
    }

    public boolean borrarCategoria (Long id){

        categoriaRepository.deleteById(id);
        return true;
    }

}
