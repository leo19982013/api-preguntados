package com.lalonso.preguntados.services;

import com.lalonso.preguntados.dto.Pregunta_RespuestaDTO;
import com.lalonso.preguntados.dto.RespuestaDTO;
import com.lalonso.preguntados.dto.ValidacionRespuestaDTO;
import com.lalonso.preguntados.models.PreguntaModel;
import com.lalonso.preguntados.models.RespuestaModel;
import com.lalonso.preguntados.repositories.PreguntaRepository;
import com.lalonso.preguntados.repositories.PreguntadosRepository;
import com.lalonso.preguntados.repositories.RespuestaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PreguntadosService {


    private RespuestaRepository respuestaRepository;
    private PreguntaRepository preguntaRepository;

    public PreguntadosService(
            PreguntadosRepository preguntadosRepository,
            RespuestaRepository respuestaRepository,
            PreguntaRepository preguntaRepository) {
        this.respuestaRepository = respuestaRepository;
        this.preguntaRepository = preguntaRepository;

    }


    public Pregunta_RespuestaDTO ObtenerPregunta() {

        Pregunta_RespuestaDTO pregunta_respuestaDTO = new Pregunta_RespuestaDTO();
        Optional<PreguntaModel> preguntaModelOptional;
        PreguntaModel preguntaModel=new PreguntaModel();
        Long cantidadPreguntasDb = this.preguntaRepository.count();

        if (cantidadPreguntasDb == 0)
            return null;

        int NumeroRandom;
        List<RespuestaModel> respuestaModelList = new ArrayList();
        do {
            NumeroRandom = (int) (Math.random() * (cantidadPreguntasDb + 1));
            preguntaModelOptional = preguntaRepository.findById((long) NumeroRandom);


            respuestaModelList = this.respuestaRepository.finAllByPreguntaId((long) NumeroRandom);

        } while (NumeroRandom == 0 || Objects.isNull(preguntaModelOptional));

        preguntaModel = preguntaModelOptional.get();

        List<RespuestaDTO> respuestaDTOList = new ArrayList<>();
        for ( RespuestaModel respuestaModel : respuestaModelList){
            RespuestaDTO respuestaDTO = new RespuestaDTO();
            respuestaDTO.setTexto(respuestaModel.getTexto());
            respuestaDTO.setRespuesta_id(respuestaModel.getRespuestaId());
            respuestaDTO.setEsCorrecta(respuestaModel.isEsCorrecta());
            respuestaDTOList.add(respuestaDTO);

        }


        pregunta_respuestaDTO.setEnunciado(preguntaModel.getEnunciado());
        pregunta_respuestaDTO.setPregunta_id(preguntaModel.getId());
        pregunta_respuestaDTO.setOpciones(respuestaDTOList);

        return pregunta_respuestaDTO;


    }

    public ValidacionRespuestaDTO validarRespuesta(Long id){

        Optional<RespuestaModel> respuestaModelOptional = this.respuestaRepository.findById(id);
        RespuestaModel respuestaModel = new RespuestaModel();
        respuestaModel = respuestaModelOptional.get();

        ValidacionRespuestaDTO validacionRespuestaDTO = new ValidacionRespuestaDTO();

        validacionRespuestaDTO.setRespuesta_id(id);
        validacionRespuestaDTO.setEs_correcto(respuestaModel.isEsCorrecta());

        return validacionRespuestaDTO;
    }


}
