package com.lalonso.preguntados.services;

import com.lalonso.preguntados.dto.PreguntaDTO;
import com.lalonso.preguntados.dto.RespuestaDTO;
import com.lalonso.preguntados.models.PreguntaModel;
import com.lalonso.preguntados.models.RespuestaModel;
import com.lalonso.preguntados.repositories.PreguntaRepository;
import com.lalonso.preguntados.repositories.RespuestaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PreguntaService {

    private PreguntaRepository preguntaRepository;
    private RespuestaRepository respuestaRepository;


    public PreguntaService(PreguntaRepository preguntaRepository, RespuestaRepository respuestaRepository) {

        this.preguntaRepository = preguntaRepository;
        this.respuestaRepository = respuestaRepository;

    }

    public ArrayList<PreguntaModel> obtenerPreguntas(){
        return (ArrayList<PreguntaModel>) this.preguntaRepository.findAll();
    }

    public PreguntaModel crearPregunta (PreguntaDTO preguntaDTO){

        PreguntaModel preguntaModel = new PreguntaModel();
        preguntaModel.setEnunciado(preguntaDTO.getEnunciado());
        preguntaModel.setCategoria_id(preguntaDTO.getCategoria_id());
        preguntaRepository.save(preguntaModel);

        for (RespuestaDTO respuestaDTO : preguntaDTO.getOpciones()){

            RespuestaModel respuestaModel = new RespuestaModel();
            respuestaModel.setTexto(respuestaDTO.getTexto());
            respuestaModel.setEsCorrecta(respuestaDTO.getisEsCorrecta());
            respuestaModel.setPreguntaId(preguntaModel.getId());
            respuestaRepository.save(respuestaModel);
        }

        return null;

    }

    public Optional<PreguntaModel> obtenerPreguntaPorId(Long id){
        return preguntaRepository.findById(id);
    }

    public void borrarPregunta(Long id){
        preguntaRepository.deleteById(id);
    }



}
