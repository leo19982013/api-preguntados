package com.lalonso.preguntados.models;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Pregunta_RespuestaModel")
public class Pregunta_RespuestaModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pregunta_respuesta_id")
    private Long pregunta_respuesta_id;
    private Long pregunta_id;
    private Long respuesta_id;

}
