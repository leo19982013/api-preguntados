package com.lalonso.preguntados.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "respuestas")

public class RespuestaModel {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "respuesta_id")
   private Long respuestaId;

   private String texto;

   @Column(name = "es_correcta")
   private boolean esCorrecta;

   private Long preguntaId;

}
