package com.lalonso.preguntados.dto;

import java.util.ArrayList;
import java.util.List;

public class Pregunta_RespuestaDTO {

    private Long pregunta_id;
    private String enunciado;

    private List<RespuestaDTO> opciones= new ArrayList();

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public List<RespuestaDTO> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<RespuestaDTO> opciones) {
        this.opciones = opciones;
    }

    public Long getPregunta_id() {
        return pregunta_id;
    }

    public void setPregunta_id(Long pregunta_id) {
        this.pregunta_id = pregunta_id;
    }
}
