package com.lalonso.preguntados.dto;

public class ValidacionRespuestaDTO {

    private Long respuesta_id;

    private boolean es_correcto;

    public Long getRespuesta_id() {
        return respuesta_id;
    }

    public void setRespuesta_id(Long respuesta_id) {
        this.respuesta_id = respuesta_id;
    }

    public boolean getEs_correcto() {
        return es_correcto;
    }

    public void setEs_correcto(boolean es_correcto) {
        this.es_correcto = es_correcto;
    }
}
