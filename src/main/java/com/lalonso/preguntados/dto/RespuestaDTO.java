package com.lalonso.preguntados.dto;

public class RespuestaDTO {

    private Long respuesta_id;
    private String texto;
    private boolean esCorrecta;

    public Long getRespuesta_id() {
        return respuesta_id;
    }

    public void setRespuesta_id(Long respuesta_id) {
        this.respuesta_id = respuesta_id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public boolean getisEsCorrecta() {
        return esCorrecta;
    }

    public void setEsCorrecta(boolean esCorrecta) {
        this.esCorrecta = esCorrecta;
    }
}
