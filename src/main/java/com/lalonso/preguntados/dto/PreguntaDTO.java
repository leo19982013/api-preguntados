package com.lalonso.preguntados.dto;

import java.util.ArrayList;
import java.util.List;

public class PreguntaDTO {

    private Long categoria_id;
    private String enunciado;

    private List<RespuestaDTO> opciones= new ArrayList();

    public List<RespuestaDTO> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<RespuestaDTO> opciones) {
        this.opciones = opciones;
    }

    public Long getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(Long categoria_id) {
        this.categoria_id = categoria_id;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }
}
